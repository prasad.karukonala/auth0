import React, { useEffect } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useAppContext } from "./context";
import { protectedRoutes, unProtectedRoutes } from "./routes";

export default function App() {
  const { state, services } = useAppContext();

  useEffect(() => {
    console.log('Initial render')
    services.tokenListener();
  }, []);

  if (state.isLoading) return <div>Loading...</div>;

  if (state.err) return <div>{JSON.stringify(state.err)}</div>;

  if (!state.isLoggedIn) {
    return (
      <BrowserRouter>
        <Switch>
          {unProtectedRoutes.map(({ path, component }) => {
            return (
              <Route
                key={path}
                exact={true}
                path={path}
                component={component}
              />
            );
          })}
          <Redirect from="/" to="/login" />
        </Switch>
      </BrowserRouter>
    );
  }

  return (
    <BrowserRouter>
      <Switch>
        {protectedRoutes.map(({ path, component }) => {
          return (
            <Route key={path} exact={true} path={path} component={component} />
          );
        })}
        <Redirect from="/login" to="/profile" />
      </Switch>
    </BrowserRouter>
  );
}
