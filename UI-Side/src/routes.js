import LoginCallback from './components/callback';
import Login from './components/login';
import Profile from './components/profile';
import Register from './components/register';

const protectedRoutes = [
  {
    path: "/profile",
    component: Profile,
  },
]

const unProtectedRoutes = [
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/login/callback",
    component: LoginCallback
  },
  {
    path: "/register",
    component: Register,
  },
];

export { protectedRoutes, unProtectedRoutes }