import React, { useState, } from "react";
import { useAppContext } from "../../context";
import Presentation from "./Presentation";

export const Container = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { services } = useAppContext()

  const onLogin = (e) => {
    e.preventDefault();
    services.login(email, password)
  };

  const handleChange = (e) => {
    setEmail(e.target.value);
  };
  
  return (
    <div>
      <Presentation
        onLogin={onLogin}
        handleChange={handleChange}
        setPassword={setPassword}
        email={email}
        password={password}
      />
    </div>
  );
};



export default Container;
