// eslint-disable-next-line no-unused-vars
import React from "react";
import {
  Avatar,
  CssBaseline,
  Grid,
  Box,
  Typography,
  Container,
  IconButton,
  InputAdornment,
  Link,
  makeStyles,
  TextField,
  Button,
} from "@material-ui/core";
import {
  LockOutlined as LockOutlinedIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import GoogleButton from "react-google-button";
// import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
// import FormButton from "../../../../shared/components/formfields/FormButton";
import PropTypes from "prop-types";
// import EmailInput from "../../../../shared/components/formfields/EmailInput";
// import LoginForm from "../../../../assets/LoginFormimg.svg";
import clsx from "clsx";
// import useMediaQuery from "@material-ui/core/useMediaQuery";
import Slide from "@material-ui/core/Slide";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props' 
import { useAppContext } from "../../context";


const height = window.innerHeight;
const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
    marginTop: "30px",
    opacity: 1,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },

  loginform: {
    height: height * 0.53 + "px",
    borderRadius: "10px",
    overflow: "hidden",
    // backgroundImage: `url(${LoginForm})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    width: height * 0.6 + "px",
    backgroundColor: "#e0e0e0",
    marginLeft: "2vw",
    fill: theme.palette.common.white,
    stroke: theme.palette.divider,
    strokeWidth: 3,
  },
  mobileView_loginform: {
    height: height * 0.7 + "px",
    borderRadius: "10px",
    overflow: "hidden",
    // backgroundImage: `url(${LoginForm})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    width: height * 0.52 + "px",
    backgroundColor: "#e0e0e0",
    marginLeft: "4vw",
    fill: theme.palette.common.white,
    stroke: theme.palette.divider,
    strokeWidth: 1,
  },
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -12,
  },
  buttonRoot: {
    display: "flex",
    alignItems: "center",
  },
  buttonStyles: {
    width: "100%",
    backgroundColor: "#47cf73",
    fontWeight: 600,
  },
}));

function Copyright() {
  return (
    <Typography variant="body2" color="primary" align="center">
      {"Copyright © "}
      <Link color="primary" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function Presentation(props) {
  const { email, password, handleChange, setPassword, onLogin } = props;
  const classes = useStyles();
  const {services} = useAppContext()
  //   const isProcessing = props.auth.login.isProcessing;
  //   const matches = useMediaQuery("(min-width:600px)");
  const [showPassword, setShowPassword] = React.useState(false);
  const [checked] = React.useState(true);
  return (
    <div className="login-bg" style={{ height: "100vh" }}>
      <br />
      <br />
      <br />
      <br />
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}></Grid>
        <Grid item xs={12} sm={4}>
          <Slide direction="up" in={checked} mountOnEnter unmountOnExit>
            <div style={{ position: "relative" }}>
              <div className={clsx(classes.mobileView_loginform)}>
                <div>
                  <Container component="main">
                    <CssBaseline />
                    <div>
                      <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                          <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                          Sign in To Atelia
                        </Typography>
                        <form onSubmit={onLogin} className={classes.form}>
                          <TextField
                            label="Email Address"
                            name="email"
                            value={email}
                            onChange={handleChange}
                            required
                            variant="outlined"
                            fullWidth
                            margin="normal"
                            id="log-in-email-address"
                          />
                          <TextField
                            variant="outlined"
                            required
                            fullWidth
                            margin="normal"
                            name="password"
                            label="Password"
                            type={showPassword ? "text" : "password"}
                            id="log-in-password"
                            autoComplete="current-password"
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                            size="small"
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton edge="end">
                                    {showPassword ? (
                                      <VisibilityIcon
                                        onClick={() =>
                                          setShowPassword(!showPassword)
                                        }
                                      />
                                    ) : (
                                      <VisibilityOffIcon
                                        onClick={() =>
                                          setShowPassword(!showPassword)
                                        }
                                      />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                          />
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            // disabled={isProcessing}
                            fullWidth
                            className={classes.buttonStyles}
                          >
                            SIGN IN
                          </Button>

                          <Grid container>
                            <Grid item xs>
                              <Link
                                href="/register"
                                variant="body2"
                                style={{ textAlign: "end " }}
                              >
                                <div> New User Register Now ?</div>
                              </Link>
                            </Grid>
                          </Grid>

                          <Grid container>
                            <Grid item xs>
                              <a href="http://localhost:8000/loginwithgoogle">
                              <GoogleButton
                              />
                              </a>
                            </Grid>
                          </Grid>
                          &nbsp;
                          &nbsp;
                          &nbsp;
                        
                        </form>
                      </div>
                      <div style={{ marginBottom: "30px" }}>
                        <Box mt={8}>
                          <Copyright />
                        </Box>
                      </div>
                    </div>
                  </Container>
                </div>
              </div>
            </div>
          </Slide>
        </Grid>
        <Grid item xs={12} sm={3}></Grid>
      </Grid>
    </div>
  );
}

Presentation.propTypes = {
  auth: PropTypes.shape({
    login: PropTypes.shape({
      isProcessing: PropTypes.any,
    }),
  }),
  email: PropTypes.string,
  onLogin: PropTypes.func,
  password: PropTypes.string,
  handleChange: PropTypes.func,
  setPassword: PropTypes.func,
  showPassword: PropTypes.bool,
};

export default Presentation;
