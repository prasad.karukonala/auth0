import React, { useState } from "react";
import {
  Avatar,
  CssBaseline,
  Grid,
  Box,
  Typography,
  Container,
  IconButton,
  InputAdornment,
  Link,
  makeStyles,
  TextField,
  Button,
} from "@material-ui/core";
import { green } from "@material-ui/core/colors";
// import FormButton from "../../../../shared/components/formfields/FormButton";
import PropTypes from "prop-types";
// import EmailInput from "../../../../shared/components/formfields/EmailInput";
// import LoginForm from "../../../../assets/LoginFormimg.svg";
import clsx from "clsx";
// import useMediaQuery from "@material-ui/core/useMediaQuery";
import Slide from "@material-ui/core/Slide";
import {
  LockOutlined as LockOutlinedIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { useAppContext } from "../../context";
export default function Register() {
  const {services} = useAppContext()
  const [registerDetails, setRegisterDeatails] = useState({
    fullName: "",
    userName: "",
    email: "",
    password: "",
  });
  const height = window.innerHeight;

  const handleChange = (e) => {
    setRegisterDeatails({
      ...registerDetails,
      [e.target.name]: e.target.value,
    });
  };
  const onRegister = (e) => {
    e.preventDefault();
    console.log(registerDetails);
    const payload=registerDetails
    services.register(payload)
    
  };
  const [showPassword, setShowPassword] = React.useState(false);
  const [checked] = React.useState(true);
  const useStyles = makeStyles((theme) => ({
    paper: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
      marginTop: "30px",
      opacity: 1,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },

    loginform: {
      height: height * 0.53 + "px",
      borderRadius: "10px",
      overflow: "hidden",
      // backgroundImage: `url(${LoginForm})`,
      backgroundSize: "cover",
      backgroundPosition: "center",
      width: height * 0.6 + "px",
      backgroundColor: "#e0e0e0",
      marginLeft: "2vw",
      fill: theme.palette.common.white,
      stroke: theme.palette.divider,
      strokeWidth: 3,
    },
    mobileView_loginform: {
      height: height * 0.7 + "px",
      borderRadius: "10px",
      overflow: "hidden",
      // backgroundImage: `url(${LoginForm})`,
      backgroundSize: "cover",
      backgroundPosition: "center",
      width: height * 0.52 + "px",
      backgroundColor: "#e0e0e0",
      marginLeft: "4vw",
      fill: theme.palette.common.white,
      stroke: theme.palette.divider,
      strokeWidth: 1,
    },
    wrapper: {
      margin: theme.spacing(1),
      position: "relative",
    },
    buttonProgress: {
      color: green[500],
      position: "absolute",
      top: "50%",
      left: "50%",
      marginLeft: -12,
    },
    buttonRoot: {
      display: "flex",
      alignItems: "center",
    },
    buttonStyles: {
      width: "100%",
      backgroundColor: "#47cf73",
      fontWeight: 600,
    },
  }));
  const classes = useStyles();

  return (
    <div className="login-bg" style={{ height: "100vh" }}>
      <br />
      <br />
      <br />
      <br />
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}></Grid>
        <Grid item xs={12} sm={4}>
          <Slide direction="up" in={checked} mountOnEnter unmountOnExit>
            <div style={{ position: "relative" }}>
              <div className={clsx(classes.mobileView_loginform)}>
                <div>
                  <Container component="main">
                    <CssBaseline />
                    <div>
                      <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                          <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                          Register To Atelia
                        </Typography>
                        <form onSubmit={onRegister} className={classes.form}>
                          <TextField
                            label="Full Name"
                            name="fullName"
                            value={registerDetails.fullName}
                            onChange={handleChange}
                            required
                            variant="outlined"
                            fullWidth
                            margin="normal"
                            id="log-in-email-address"
                          />
                          <TextField
                            label="User Name"
                            name="userName"
                            value={registerDetails.userName}
                            onChange={handleChange}
                            required
                            variant="outlined"
                            fullWidth
                            margin="normal"
                            id="log-in-email-address"
                          />
                          <TextField
                            label="Email Address"
                            name="email"
                            value={registerDetails.email}
                            onChange={handleChange}
                            required
                            variant="outlined"
                            fullWidth
                            margin="normal"
                            id="log-in-email-address"
                          />
                          <TextField
                            variant="outlined"
                            required
                            fullWidth
                            margin="normal"
                            name="password"
                            label="Password"
                            type={showPassword ? "text" : "password"}
                            id="log-in-password"
                            autoComplete="current-password"
                            onChange={handleChange}
                            value={registerDetails.password}
                            size="small"
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton edge="end">
                                    {showPassword ? (
                                      <VisibilityIcon
                                        onClick={() =>
                                          setShowPassword(!showPassword)
                                        }
                                      />
                                    ) : (
                                      <VisibilityOffIcon
                                        onClick={() =>
                                          setShowPassword(!showPassword)
                                        }
                                      />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                          />
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            // disabled={isProcessing}
                            fullWidth
                            className={classes.buttonStyles}
                          >
                            Register for atelia{" "}
                          </Button>
                        </form>
                      </div>
                    </div>
                  </Container>
                </div>
              </div>
            </div>
          </Slide>
        </Grid>
        <Grid item xs={12} sm={3}></Grid>
      </Grid>
    </div>
  );
}
