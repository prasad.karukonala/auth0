import React from "react";
import { useAppContext } from "../../context";

export default function Profile() {
  const { state } = useAppContext();
  return (
    <div>
      <h1>Name :{state.userData.name}</h1>
      <h1>email :{state.userData.email}</h1>
    </div>
  );
}
