import axios from "axios";
import { createContext, useContext, useState } from "react";
import { useHistory } from "react-router-dom";

const AppContext = createContext();

const useAppContext = () => useContext(AppContext);

const initialState = {
  isLoggedIn: false,
  isLoading: true,
  err: null,
  userData: {},
  isRegistered: false,
};

function AppContextProvider(props) {
  const history = useHistory();
  const [isLoggedIn, setIsLoggedIn] = useState(initialState.isLoggedIn);
  const [userData, setUserData] = useState(initialState.userData);
  const [isLoading, setIsLoading] = useState(initialState.isLoading);
  const [isRegistered, setIsRegistered] = useState(initialState.isLoading);

  const [err, setErr] = useState(initialState.err);

  const state = {
    isLoggedIn,
    userData,
    isLoading,
    err,
    isRegistered,
  };

  const stateSetters = {
    setIsLoggedIn,
    setUserData,
    setIsLoading,
    setErr,
    setIsRegistered,
  };

  const tokenListener = async () => {
    setIsLoading(true);
    const localAccessToken = localStorage.getItem("accessToken");

    if (localAccessToken) {
      try {
        const userdata = await axios.get(
          "https://dev-j3t1d7cv.us.auth0.com/userinfo",
          {
            headers: {
              Authorization: `Bearer ${localAccessToken}`,
            },
          }
        );
        console.log({ localAccessToken, userdata });
        setIsLoading(false);
        setIsLoggedIn(true);
        setUserData(userdata.data);
      } catch (error) {
        setIsLoading(false);
        setIsLoggedIn(false);
        setUserData({});
      }
    } else {
      setIsLoading(false);
      setIsLoggedIn(false);
      setUserData({});
    }
  };


  // const getuserbyemail = () => {
  //   try {
  //     const headers = {
  //       "Content-Type": "application/json",
  //     };
  //     const postdata = await axios.get(
  //       "https://dev-j3t1d7cv.us.auth0.com/api/v2/users-by-email",
  //        {email: 'saiprasad9467@gmail.com'},
  //       { headers: headers }
  //     );
  //     const accessToken = postdata.data.access_token;
  //     const headers1 = {
  //       Authorization: "Bearer " + postdata.data.access_token,
  //     };
  //     localStorage.setItem("accessToken", accessToken);
  //     const userdata = await axios.get(
  //       "https://dev-j3t1d7cv.us.auth0.com/userinfo",
  //       { headers: headers1 }
  //     );
  //     // res.send(userdata.data)
  //     console.log(userdata.data);
  //     setIsLoading(false);
  //     setIsLoggedIn(true);
  //     setUserData(userdata.data);
  //   } catch (error) {
  //     console.log(error);
  //     setIsLoading(false);
  //     setIsLoggedIn(false);
  //     setUserData({});
  //     setErr(error);
  //   }
  // }
  const login = async (email, password) => {
    setIsLoading(true);

    const data = {
      grant_type: "password",
      username: email,
      password: password,
      audience: "https://dev-j3t1d7cv.us.auth0.com/api/v2/",
      scope: "openid",
      client_id: "EVBqZdongsBnWq2a7jwPrRHhqdHHeycq",
      client_secret:
        "TgFQK8Ivc6iRMRBGrM3jGFkcKd2GjS9j5Vuyj_MGu-j-GDYEV4iitrPYBd3q-spt",
      turbo: "false",
    };
    const headers = {
      "Content-Type": "application/json",
    };
    console.log(data);
    try {
      const postdata = await axios.post(
        "https://dev-j3t1d7cv.us.auth0.com/oauth/token",
        data,
        { headers: headers }
      );
      const accessToken = postdata.data.access_token;
      const headers1 = {
        Authorization: "Bearer " + postdata.data.access_token,
      };
      localStorage.setItem("accessToken", accessToken);
      const userdata = await axios.get(
        "https://dev-j3t1d7cv.us.auth0.com/userinfo",
        { headers: headers1 }
      );
      // res.send(userdata.data)
      console.log(userdata.data);
      setIsLoading(false);
      setIsLoggedIn(true);
      setUserData(userdata.data);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      setIsLoggedIn(false);
      setUserData({});
      setErr(error);
    }
  };

  const register = async (payload) => {
    const { fullName, userName, email, password } = payload;
    console.log(fullName, userName, email, password);
    setIsLoading(true);
    const data = {
      client_id: "EVBqZdongsBnWq2a7jwPrRHhqdHHeycq",
      email: email,
      password: password,
      connection: "Username-Password-Authentication",
      username: userName,
      given_name: fullName,
      family_name: fullName,
      name: fullName,
      nickname: fullName,
      picture: "http://example.com/ssdsd.jpg",
      user_metadata: {},
    };
    const headers = {
      "Content-Type": "application/json",
    };
    try {
      await axios.post(
        "https://dev-j3t1d7cv.us.auth0.com/dbconnections/signup",
        data,
        { headers: headers }
      );
      setIsRegistered(true);
      history.push("/login");
    } catch (error) {
      console.log(error);
    }
  };

  const googleRegister = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get("http://localhost:8000/loginwithgoogle");
      console.log(response);
      setIsLoading(false);
      setIsLoggedIn(true);
      setUserData(response.data);
    } catch (error) {
      console.error(error);
      setIsLoading(false);
      setIsLoggedIn(false);
      setUserData({});
    }
  };

  const services = {
    login,
    tokenListener,
    register,
    googleRegister,
  };

  return (
    <AppContext.Provider value={{ state, stateSetters, services }}>
      {props.children}
    </AppContext.Provider>
  );
}

export { AppContextProvider, useAppContext };
