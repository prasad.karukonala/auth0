const express = require('express')
const app = express()
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.set('view engine', 'ejs');
const axios = require('axios');
const session = require('express-session');
const flash = require('connect-flash');
const morgan = require('morgan')
const passport = require('passport');
const cookieParser = require('cookie-parser');
const cors = require('cors')
app.use(cors({ origin: true }))
app.use(session({
    secret: 'praneeth',
    saveUninitialized: true,
    resave: true
}));
app.use(flash());
app.get('/', (req, res) => res.render('index', { "msg": req.flash('message') }))
app.get('/register', (req, res) => res.render('register'))
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', req.header('origin'));
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Credentials', 'true');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
});
app.post('/register', async (req, res) => {
    const { FullName, Username, EmailAddress, Password } = req.body
    const data = {
        "client_id": "EVBqZdongsBnWq2a7jwPrRHhqdHHeycq",
        "email": EmailAddress,
        "password": Password,
        "connection": "Username-Password-Authentication",
        "username": Username,
        "given_name": FullName,
        "family_name": FullName,
        "name": FullName,
        "nickname": FullName,
        "picture": "http://example.com/ssdsd.jpg",
        "user_metadata": {}
    }
    const headers = {
        'Content-Type': 'application/json'
    }
    try {
        let postdata = await axios.post('https://dev-j3t1d7cv.us.auth0.com/dbconnections/signup', data, { headers: headers });
        if (postdata.status == 200) {
            req.flash('message', "Register Successfully! Login Now...");
            res.redirect('/')
        }
    } catch (error) {
        console.log(error)
    }

})
app.post('/login', async (req, res) => {
    const { EmailAddress, Password } = req.body
    const data = {
        "grant_type": "password",
        "username": EmailAddress,
        "password": Password,
        "audience": "https://dev-j3t1d7cv.us.auth0.com/api/v2/",
        "scope": "openid",
        "client_id": "EVBqZdongsBnWq2a7jwPrRHhqdHHeycq",
        "client_secret": "TgFQK8Ivc6iRMRBGrM3jGFkcKd2GjS9j5Vuyj_MGu-j-GDYEV4iitrPYBd3q-spt"
    }
    const headers = {
        'Content-Type': 'application/json',

    }
    try {
        let postdata = await axios.post('https://dev-j3t1d7cv.us.auth0.com/oauth/token', data, { headers: headers });
        if (postdata.status == 200) {
            const headers1 = {
                "Authorization": "Bearer " + postdata.data.access_token
            }
            let userdata = await axios.get('https://dev-j3t1d7cv.us.auth0.com/userinfo', { headers: headers1 });
            // res.send(userdata.data)
            console.log(userdata.data)
        }
    } catch (error) {
        console.log(error)
    }

})
const config = require('./config/config')
require('./config/passport')(passport, config);

app.use(passport.initialize());
app.use(passport.session());
require('./config/routes')(app, config, passport);
app.use(cookieParser());
app.use(morgan('dev'))
app.listen(8000, () => {
    console.log('app listening on http://localhost:8000')
})