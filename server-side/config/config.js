const fs = require('fs');
module.exports = {
  development: {
    app: {
      name: 'Passport SAML strategy example',
      port: process.env.PORT || 3000
    },
    google: {
      strategy: 'google',
      saml: {
        path: process.env.SAML_PATH || '/login/callback',
        entryPoint: process.env.SAML_ENTRY_POINT || 'https://dev-j3t1d7cv.us.auth0.com/samlp/EVBqZdongsBnWq2a7jwPrRHhqdHHeycq?connection=google-oauth2',
        issuer: 'dev-j3t1d7cv.us.auth0.com',
        cert: process.env.SAML_CERT || fs.readFileSync('awsssocert.pem','utf8'),
        acceptedClockSkewMs: -1
      }
    },
    facebook: {
      strategy: 'Facebook',
      saml: {
        path: process.env.SAML_PATH || '/login/callback',
        entryPoint: process.env.SAML_ENTRY_POINT || 'https://dev-j3t1d7cv.us.auth0.com/samlp/EVBqZdongsBnWq2a7jwPrRHhqdHHeycq?connection=facebook',
        issuer: 'dev-j3t1d7cv.us.auth0.com',
        cert: process.env.SAML_CERT || fs.readFileSync('awsssocert.pem','utf8'),
        acceptedClockSkewMs: -1
      }
    }
  }
};
