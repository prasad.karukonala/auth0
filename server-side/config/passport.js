const SamlStrategy = require('passport-saml').Strategy;
const fs = require('fs');

module.exports = function (passport, config) {

  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  passport.use('google',new SamlStrategy(
    {
      path: '/login/callback',
      entryPoint: process.env.SAML_ENTRY_POINT || 'https://dev-j3t1d7cv.us.auth0.com/samlp/EVBqZdongsBnWq2a7jwPrRHhqdHHeycq?connection=google-oauth2',
      issuer: 'dev-j3t1d7cv.us.auth0.com',
      cert: process.env.SAML_CERT || fs.readFileSync('awsssocert.pem','utf8'),
      acceptedClockSkewMs: -1
    },
    function (profile, done) {
      console.log(profile)
      return done(null,
        {
          id: profile,
          email: profile.nameID,
          displayName: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'],
          firstName: profile.name,
          lastName: profile.surname
        });
    })
  );
  passport.use('facebook',new SamlStrategy(
    config.development.facebook.saml,
    function (profile, done) {
      console.log(profile)
      return done(null,
        {
          id: profile,
          email: profile.nameID,
          displayName: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'],
          firstName: profile.name,
          lastName: profile.surname
        });
    })
  );

};
