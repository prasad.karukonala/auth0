
module.exports = function (app, config, passport) {

    app.get('/interface', function (req, res) {
      if (req.isAuthenticated()) {
        res.render('profile',
          {
            user: req.user
          });
      } else {
        res.render('profile',
          {
            user: null
          });
      }
    });
    app.get('/getdetail', function (req, res) {
      console.log("Yes",req.user)
      if (req.isAuthenticated()) {
        console.log(req.user.id,"details")
        return  res.json(req.user);
      } else {
        res.json({Message: "No Data Found"})
      }
    });
    app.get('/loginwithfacebook',
    passport.authenticate('facebook',
      {
        successRedirect: 'http://localhost:3000/login/callback?userId=123',
        failureRedirect: '/loginwithfacebook'
      })
    );

    app.get('/loginwithgoogle',
      passport.authenticate('google',
        {
          successRedirect: 'http://localhost:3000/login/callback?userId=123',
          failureRedirect: '/loginwithgoogle'
        })
      );
  
    app.post(config.development.google.saml.path,
      passport.authenticate(config.development.google.strategy,
        {
          failureRedirect: '/',
          failureFlash: true
        }),
      function (req, res) {
        // console.log(req.user)
        res.redirect(`http://localhost:3000/login/callback?userId=${req.user.email}`);
      }
    );
  
    app.get('/logout', (req,res)=>{ req.session.destroy(function (err) {
      res.redirect('/'); 
    })})

  };
  